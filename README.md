theme-material-dashboard
==============================================================================

[Short description of the addon.]

Installation
------------------------------------------------------------------------------

```
ember install theme-material-dashboard
```


Usage
------------------------------------------------------------------------------

[Longer description of how to use the addon in apps.]


License
------------------------------------------------------------------------------

This project is licensed under the [MIT License](LICENSE.md).
