'use strict';

module.exports = {
  name: require('./package').name,

  contentFor(type, config) {
    if (type === 'head') {
      return '<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />'
      + '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">';
    } else if (type === 'head-footer') {
      return '';
    } else if (type === 'body') {
      return '';
    } else if (type === 'body-footer') {
      return '';
    };
  }
};
